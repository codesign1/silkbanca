<?php

namespace silkbanca\app\controllers;

use Velocity\Config\Config;
use Velocity\Core\Controller;
use Velocity\Helpers\Helpers;
use Velocity\Authentication\Cookie;

class HomeCtrl extends Controller {

	public  $variable,
			$meta_description,
			$meta_keywords,
			$meta_autor,
			$lang,
			$mensaje,
			$mensaje_below,
			$foto_martha,
			$mensaje_martha;

	public function init() {
		$this->meta_description = 'Algo';
		$this->meta_keywords = 'Algo';
		$this->meta_autor = 'Algo';
		$this->lang = Cookie::get('idioma');
		$this->twitter = $this->cms->query("SELECT * FROM wp_posts WHERE post_status = 'publish' AND post_type = 'twitter'");
		if($this->lang=='es') {
			$this->mensaje = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 7")[0]->post_content;
			$this->mensaje_below = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 8")[0]->post_content;
			$this->mensaje_martha = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 9")[0]->post_content;		
		} else {
			$this->mensaje = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 12")[0]->post_content;
			$this->mensaje_below = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 14")[0]->post_content;
			$this->mensaje_martha = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 15")[0]->post_content;	
		}
		$this->foto_martha = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 10")[0]->post_content;
			
		if($this->lang=='es') {
			$this->ser1 = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 17")[0]->post_title;
			$this->ser12 = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 50")[0]->meta_value;
			$this->ser13 = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 52")[0]->meta_value;
			$this->ser2 = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 26")[0]->post_title;
			$this->ser22 = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 59")[0]->meta_value;
			$this->ser23 = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 61")[0]->meta_value;
			$this->ser3 = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 27")[0]->post_title;
			$this->ser32 = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 67")[0]->meta_value;
			$this->ser33 = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 69")[0]->meta_value;
			$this->ser4 = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 28")[0]->post_title;
			$this->ser42 = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 75")[0]->meta_value;
			$this->ser43 = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 77")[0]->meta_value;
			$this->ser5 = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 29")[0]->post_title;
			$this->ser52 = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 82")[0]->meta_value;
			$this->ser53 = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 84")[0]->meta_value;
		} else {
			$this->ser1 = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 30")[0]->post_title;
			$this->ser12 = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 91")[0]->meta_value;
			$this->ser13 = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 93")[0]->meta_value;
			$this->ser2 = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 31")[0]->post_title;
			$this->ser22 = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 99")[0]->meta_value;
			$this->ser23 = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 101")[0]->meta_value;
			$this->ser3 = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 32")[0]->post_title;
			$this->ser32 = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 107")[0]->meta_value;
			$this->ser33 = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 109")[0]->meta_value;
			$this->ser4 = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 33")[0]->post_title;
			$this->ser42 = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 115")[0]->meta_value;
			$this->ser43 = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 117")[0]->meta_value;
			$this->ser5 = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 34")[0]->post_title;
			$this->ser52 = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 123")[0]->meta_value;
			$this->ser53 = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 125")[0]->meta_value;
		}


	}

}
