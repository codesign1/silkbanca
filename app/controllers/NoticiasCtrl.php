<?php

namespace silkbanca\app\controllers;

use Velocity\Config\Config;
use Velocity\Core\Controller;
use Velocity\Helpers\Helpers;
use Velocity\Authentication\Cookie;

class NoticiasCtrl extends Controller {

	public  $variable,
			$meta_description,
			$meta_keywords,
			$meta_autor,
			$noticia,
			$accion,
			$noticias,
			$lang;


	public function init() {
		$this->meta_description = 'Algo';
		$this->meta_keywords = 'Algo';
		$this->meta_autor = 'Algo';
		$this->accion = '';
		$this->lang = Cookie::get('idioma');
		$this->twitter = $this->cms->query("SELECT * FROM wp_posts WHERE post_status = 'publish' AND post_type = 'twitter'");
		$this->get_posts();
	}

	public function get_posts(){
		
		if($this->lang=='es') {
			$noticias = $this->cms->query("SELECT * FROM wp_posts WHERE post_status = 'publish' AND post_type = 'blog'");
		} else {
			$noticias = $this->cms->query("SELECT * FROM wp_posts WHERE post_status = 'publish' AND post_type = 'blog_ingles'");

		}

		foreach ($noticias as $key) {
			$id = $key->ID;
			$title = $key->post_title;
			$date = $key->post_date;
			$content = $key->post_content;
			$more = $this->cms->query("SELECT * FROM wp_postmeta WHERE post_id = $id");
			$sumario = '';
			$img = '';
			$url = '';
			foreach ($more as $key2) {
				if($key2->meta_key=='sumario') {
					$sumario = $key2->meta_value;
				} elseif ($key2->meta_key=='img') {
					$img = $key2->meta_value;
				} elseif ($key2->meta_key=='url') {
					$url = $key2->meta_value;
				}
			}
			$this->noticias[] = array(
				'title' => $title,
				'date' => $date,
				'img' => $img,
				'url' => $url,
				'sumario' => $sumario,
				'content' => $content
			);
		}
	}

	public function get_noticia($url){
		$this->get_posts();
		$this->accion = 'detalle';
		foreach ($this->noticias as $key) {
			if($key['url']==$url) {
				$this->noticia = $key;
			}
		}
	}

}
