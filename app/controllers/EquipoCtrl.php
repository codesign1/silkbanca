<?php

namespace silkbanca\app\controllers;

use Velocity\Config\Config;
use Velocity\Core\Controller;
use Velocity\Helpers\Helpers;
use Velocity\Authentication\Cookie;

class EquipoCtrl extends Controller {

	public  $variable,
			$meta_description,
			$meta_keywords,
			$meta_autor,
			$equipo,
			$accion,
			$person,
			$lang;

	public function init() {

		$this->accion = 'Equipo';
		$this->meta_description = 'Algo';
		$this->meta_keywords = 'Algo';
		$this->meta_autor = 'Algo';
		$this->lang = Cookie::get('idioma');
		$this->twitter = $this->cms->query("SELECT * FROM wp_posts WHERE post_status = 'publish' AND post_type = 'twitter'");
		if($this->lang=='es') {
			$this->texto = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 99")[0]->post_content;
		} else {
			$this->texto = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 100")[0]->post_content;
		}

		$this->equipo = $this->get_personas('equipo');
		$this->socios = $this->get_personas('socios');
		$this->asesores = $this->get_personas('asesores');
		
	}

	public function get_personas($tipo){
		$noticias = $this->cms->query("SELECT * FROM wp_posts WHERE post_status = 'publish' AND post_type = '$tipo'");
		foreach ($noticias as $key) {
			$id = $key->ID;
			if($id != 99 && $id != 100) {
				$nombre = $key->post_title;
				$biografia = $key->post_content;
				$img = '';
				$email = '';
				$url = '';
				$lenguaje = '';
				$cargo = '';
				$more = $this->cms->query("SELECT * FROM wp_postmeta WHERE post_id = $id");	
				foreach ($more as $key2) {
					if($key2->meta_key == 'img') {
						$img = $key2->meta_value;
					} elseif ($key2->meta_key == 'email') {
						$email = $key2->meta_value;
					} elseif ($key2->meta_key == 'url') {
						$url = $key2->meta_value;
					} elseif ($key2->meta_key == 'lenguaje') {
						$lenguaje = $key2->meta_value;
					} elseif ($key2->meta_key == 'cargo') {
						$cargo = $key2->meta_value;
					}
				}
				$array[] = array(
					'nombre' => $nombre,
					'biografia' => $biografia,
					'img' => $img,
					'email' => $email,
					'url' => $url,
					'lenguaje' => $lenguaje,
					'cargo' => $cargo
				);
			}
		}
		return $array;
	}

	public function get_persona($url){
		$this->accion = 'Persona';
		foreach ($this->equipo as $key) {
			if($key['url']==$url) {
				$this->person = $key;
			}
		}
		foreach ($this->socios as $key) {
			if($key['url']==$url) {
				$this->person = $key;
			}
		}
		foreach ($this->asesores as $key) {
			if($key['url']==$url) {
				$this->person = $key;
			}
		}
	}

}
