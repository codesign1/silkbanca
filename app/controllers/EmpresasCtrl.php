<?php

namespace silkbanca\app\controllers;

use Velocity\Config\Config;
use Velocity\Core\Controller;
use Velocity\Helpers\Helpers;
use Velocity\Authentication\Cookie;

class EmpresasCtrl extends Controller {

	public  $variable,
			$meta_description,
			$meta_keywords,
			$meta_autor,
			$accion,
			$contenido,
			$titulo,
			$lang,
			$url,
			$img;

	public function init() {
		$this->meta_description = 'Algo';
		$this->meta_keywords = 'Algo';
		$this->meta_autor = 'Algo';
		$this->accion = 'fusiones';
		$this->lang = Cookie::get('idioma');
		$this->twitter = $this->cms->query("SELECT * FROM wp_posts WHERE post_status = 'publish' AND post_type = 'twitter'");
		if($this->lang == 'es') {
			$this->get_servicio('fusiones-y-adquisiciones');
		} else {
			$this->get_servicio('mergers-and-acquisitions');
		}
	}

	public function get_servicio($url) {
		
		if($url == 'fusiones-y-adquisiciones') {
			$this->accion = 'fusiones';
			$this->titulo = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 17")[0]->post_title;
			$this->contenido = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 17")[0]->post_content;
			$this->url = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 52")[0]->meta_value;
			$this->img = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 54")[0]->meta_value;
		} elseif ($url == 'estructuracion-de-proyectos') {
			$this->accion = 'estructuracion';
			$this->titulo = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 26")[0]->post_title;
			$this->contenido = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 26")[0]->post_content;
			$this->url = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 61")[0]->meta_value;
			$this->img = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 63")[0]->meta_value;
		} elseif ($url == 'financiacion-y-reestructuracion-de-deuda') {
			$this->accion = 'financiacion';
			$this->titulo = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 27")[0]->post_title;
			$this->contenido = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 27")[0]->post_content;
			$this->url = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 69")[0]->meta_value;
			$this->img = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 71")[0]->meta_value;
		} elseif ($url == 'fondos-de-capital-y-agro') {
			$this->accion = 'fondos';
			$this->titulo = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 29")[0]->post_title;
			$this->contenido = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 29")[0]->post_content;
			$this->url = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 84")[0]->meta_value;
			$this->img = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 86")[0]->meta_value;
		} elseif ($url == 'mergers-and-acquisitions') {
			$this->accion = 'mergers';
			$this->titulo = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 30")[0]->post_title;
			$this->contenido = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 30")[0]->post_content;
			$this->url = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 93")[0]->meta_value;
			$this->img = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 54")[0]->meta_value;
		} elseif ($url == 'project-structuring') {
			$this->accion = 'project';
			$this->titulo = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 31")[0]->post_title;
			$this->contenido = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 31")[0]->post_content;
			$this->url = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 101")[0]->meta_value;
			$this->img = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 103")[0]->meta_value;
		} elseif ($url == 'funding-and-debt-restructuring') {
			$this->accion = 'funding';
			$this->titulo = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 32")[0]->post_title;
			$this->contenido = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 32")[0]->post_content;
			$this->url = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 109")[0]->meta_value;
			$this->img = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 111")[0]->meta_value;
		} elseif ($img == 'ipo-emision-de-acciones') {
			$this->accion = 'ipoen';
			$this->titulo = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 28")[0]->post_title;
			$this->contenido = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 28")[0]->post_content;
			$this->url = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 117")[0]->meta_value;
			$this->img = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 119")[0]->meta_value;
		} elseif ($url == 'private-and-agroindustrial-funds') {
			$this->accion = 'private';
			$this->titulo = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 34")[0]->post_title;
			$this->contenido = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 34")[0]->post_content;
			$this->url = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 125")[0]->meta_value;
			$this->img = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 127")[0]->meta_value;
		} else {
			$this->accion = 'ipo';
			$this->titulo = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 17")[0]->post_title;
			$this->contenido = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 17")[0]->post_content;
			$this->url = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 52")[0]->meta_value;
			$this->img = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 119")[0]->meta_value;
		}
		
	}

}
