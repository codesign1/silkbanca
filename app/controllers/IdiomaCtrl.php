<?php

namespace silkbanca\app\controllers;

use Velocity\Config\Config;
use Velocity\Core\Controller;
use Velocity\Helpers\Helpers;
use Velocity\Helpers\Redirect;

class IdiomaCtrl extends Controller {

	public  $variable;

	public function init() {
		$this->meta_description = 'Algo';
		$this->meta_keywords = 'Algo';
		$this->meta_autor = 'Algo';
	}

	public function cambiar_lang($lang) {
		setcookie('idioma', $lang, 0, '/');
		Redirect::to($_SERVER["HTTP_REFERER"]);
	}

}
