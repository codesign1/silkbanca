<?php

namespace silkbanca\app\controllers;

use Velocity\Config\Config;
use Velocity\Core\Controller;
use Velocity\Helpers\Helpers;
use Velocity\Authentication\Cookie;

class InversionistasCtrl extends Controller {

	public  $variable,
			$meta_description,
			$meta_keywords,
			$meta_autor,
			$sector,
			$titulo,
			$descripcion,
			$img,
			$lang,
			$macro,
			$macro_title;

	public function init() {
		$this->meta_description = 'Algo';
		$this->meta_keywords = 'Algo';
		$this->meta_autor = 'Algo';
		$this->sector = '';
		$this->lang = Cookie::get('idioma');
		$this->twitter = $this->cms->query("SELECT * FROM wp_posts WHERE post_status = 'publish' AND post_type = 'twitter'");
		if($this->lang=='es') {
			$this->macro = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 61")[0]->post_content;
			$this->macro_title = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 61")[0]->post_title;
		} else {
			$this->macro = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 62")[0]->post_content;
			$this->macro_title = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 62")[0]->post_title;
		}
		if($this->lang=='en') {
			$this->titulo1 = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 36")[0]->post_title;
			$this->titulo2 = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 50")[0]->post_title;
			$this->titulo3 = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 51")[0]->post_title;
			$this->titulo4 = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 52")[0]->post_title;
			$this->titulo5 = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 53")[0]->post_title;
			$this->titulo6 = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 54")[0]->post_title;
			$this->img1 = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 165")[0]->meta_value;
			$this->img2 = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 171")[0]->meta_value;
			$this->img3 = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 177")[0]->meta_value;
			$this->img4 = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 183")[0]->meta_value;
			$this->img5 = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 189")[0]->meta_value;
			$this->img6 = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 195")[0]->meta_value;
		} else {
			$this->titulo1 = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 55")[0]->post_title;
			$this->titulo2 = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 56")[0]->post_title;
			$this->titulo3 = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 57")[0]->post_title;
			$this->titulo4 = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 58")[0]->post_title;
			$this->titulo5 = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 59")[0]->post_title;
			$this->titulo6 = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 60")[0]->post_title;
			$this->img1 = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 201")[0]->meta_value;
			$this->img2 = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 207")[0]->meta_value;
			$this->img3 = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 213")[0]->meta_value;
			$this->img4 = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 219")[0]->meta_value;
			$this->img5 = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 225")[0]->meta_value;
			$this->img6 = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 231")[0]->meta_value;
		}
	}

	public function get_sector($url){
		$this->sector = $url;
		if($this->sector == 'agroindustria') {
			if($this->lang == 'en') {
				$this->titulo = $this->titulo2 = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 50")[0]->post_title;
				$this->descripcion = $this->titulo2 = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 50")[0]->post_content;
			} else {
				$this->titulo = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 56")[0]->post_title;
				$this->descripcion = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 56")[0]->post_content;
			}
			$this->img = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 173")[0]->meta_value;
		} elseif ($this->sector == 'infraestructura') {
			if($this->lang == 'en') {
				$this->titulo = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 36")[0]->post_title;
				$this->descripcion = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 36")[0]->post_content;
			} else {
				$this->titulo = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 55")[0]->post_title;
				$this->descripcion = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 55")[0]->post_content;
			}
			$this->img = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 203")[0]->meta_value;
		} elseif ($this->sector == 'manufactura') {
			if($this->lang == 'en') {
				$this->titulo = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 52")[0]->post_title;
				$this->descripcion = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 52")[0]->post_content;
			} else {
				$this->titulo = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 58")[0]->post_title;
				$this->descripcion = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 58")[0]->post_content;
			}
			$this->img = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 221")[0]->meta_value;
		} elseif ($this->sector == 'tic-bpo-servicios') {
			if($this->lang == 'en') {
				$this->titulo = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 53")[0]->post_title;
				$this->descripcion = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 53")[0]->post_content;
			} else {
				$this->titulo = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 59")[0]->post_title;
				$this->descripcion = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 59")[0]->post_content;
			}
			$this->img = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 227")[0]->meta_value;
		} elseif ($this->sector == 'turismo-real-estate') {
			if($this->lang == 'en') {
				$this->titulo = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 54")[0]->post_title;
				$this->descripcion = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 54")[0]->post_content;
			} else {
				$this->titulo = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 60")[0]->post_title;
				$this->descripcion = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 60")[0]->post_content;
			}
			$this->img = $this->img6 = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 231")[0]->meta_value;
		} else {
			if($this->lang == 'en') {
				$this->titulo = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 51")[0]->post_title;
				$this->descripcion = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 51")[0]->post_content;
			} else {
				$this->titulo = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 57")[0]->post_title;
				$this->descripcion = $this->cms->query("SELECT * FROM wp_posts WHERE ID = 57")[0]->post_content;
			}
			$this->img = $this->cms->query("SELECT * FROM wp_postmeta WHERE meta_id = 215")[0]->meta_value;
		}
	}

}
