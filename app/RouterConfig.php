<?php

namespace silkbanca\App;

use Velocity\Core\Router;

class RouterConfig {
	public static function execute() {
		/**
		 	* These two routes point to the same resource, i.e., controller when accessed
		 	* using a GET request
		*/
		Router::get('/', 'home');
		Router::get('/home', 'home');
		Router::get('/inversionistas', 'inversionistas');
		Router::get('/oportunidad-de-inversion', 'oportunidad');
		Router::get('/empresas', 'empresas');
		Router::get('/responsabilidad-empresarial', 'social');
		Router::get('/proyectos', 'proyectos');
		Router::get('/equipo', 'equipo');
		Router::get('/contacto', 'contacto');
		Router::get('/blog', 'noticias');
		Router::get('/idioma', 'idioma');
		
		/**
			* This is a route with basic parameters. It uses PHP regular expressions with named
			* capture groups. The capture group name is the name of the parameter sent to the
	 		* by_id method in the time controller
		*/
		Router::get('/home/(?<id>[0-9a-zA-Z-]+)', 'home#login_try');
		Router::post('/contacto/(?<id>[0-9a-zA-Z-]+)', 'contacto#send_message');
		Router::get('/idioma/(?<lang>[0-9a-zA-Z-]+)', 'idioma#cambiar_lang');
		Router::get('/equipo/(?<url>[0-9a-zA-Z-]+)', 'equipo#get_persona');
		Router::get('/blog/(?<id>[0-9a-zA-Z-]+)', 'noticias#get_noticia');
		Router::get('/inversionistas/(?<url>[0-9a-zA-Z-]+)', 'inversionistas#get_sector');
		Router::get('/oportunidad-de-inversion/(?<url>[0-9a-zA-Z-]+)', 'oportunidad#get_oportunidad');
		Router::get('/empresas/(?<url>[0-9a-zA-Z-]+)', 'empresas#get_servicio');

		/**
		 	* When using multiple parameters, they are sent in order of appearance to
		 	* the corresponding controller method
		 	*
		 	* Parameters can be defined to be of a certain type, like text or numbers only
		*/
		Router::get('/home/(?<name>[A-z]+)/hour/(?<hour>[0-9a-zA-Z-]+)/minute/(?<minute>[0-9]+)', 'home#multiple_params');
	}
}
